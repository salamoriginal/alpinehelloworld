FROM python:3.9.5

ENV PYTHONUNBUFFERED 1

ARG PORT=5000

# Copy requirements.txt first to leverage Docker caching
COPY ./webapp/requirements.txt /tmp/requirements.txt

# Install dependencies
RUN pip install --no-cache-dir -q -r /tmp/requirements.txt

# Copy the rest of the application
COPY ./webapp /opt/webapp/
WORKDIR /opt/webapp

# Create a non-root user
RUN adduser --disabled-password --gecos '' myuser

# Change the ownership of the application directory to the non-root user
RUN chown -R myuser:myuser /opt/webapp

# Switch to the non-root user
USER myuser

# Expose is not necessary for Heroku
EXPOSE 5000

# Specify the command to run the application
CMD ["gunicorn", "--bind", "0.0.0.0:5000", "wsgi"]